<!DOCTYPE HTML>
<!--
	Dimension by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>ACTIVATION PRODUCT</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
		<script src="assets/js/jquery.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/js-base64@3.7.2/base64.min.js"></script>
	</head>
	<body class="is-preload">

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Header -->
					<header id="header">
						<!-- <div class="logo">
							<img src="./images/bpom.png" alt="QR Code" style="max-width: 80%;height: auto;">
						</div> -->
						<div class="content">
							<div class="inner">
								 <h1>ACTIVATION</h1>
								 <p>halaman ini untuk melihat keaslian produk MADUHIJAU.</p>
								 <img src="./images/maduhijau.png" alt="Madu Hijau" style="max-width: 80%;height: auto;">
							</div>
						</div>
						<nav>
							<ul>
								<li><a href="#activation">CHECK QR</a></li>
								<!-- <li><a href="#elements">Elements</a></li> -->
							</ul>
						</nav>
					</header>

				<!-- Main -->
					<div id="main">
						<!-- Contact -->
							<article id="activation">
								<div style="background-color:white; max-width: 20%;text-align: center;float: right;border-radius: 25%;margin-bottom: :1rem;">
									<img src="./images/bpom.png" alt="QR Code" style="max-width: 50%;height: auto; ">
								</div>
								<h2 class="major" style="float: left;">Insert Product ID</h2>
									<div class="form">
										<div class="fields">
											<div class="field half">
													<?php
														$product_id = "";
														$product_en = "";
														if (isset($_GET["product_id"])){
															$product_id = base64_decode(htmlspecialchars($_GET["product_id"]));
															$product_en = htmlspecialchars($_GET["product_id"]);
														}
													?>
													<input type="text" name="product_id" id="product_id" value="<?php echo $product_id;?>" palceholder="PRDUCT ID" style="font-weight: bold;font-size: large; text-align:center;" readonly="true"/>
											</div>
											<div class="field half" style="margin-top:0.5rem">
			 								 		<img src="http://103.163.139.199:8000/api-oauth-service/master/barcode/v1/check/<?php echo $product_en;?>" alt="QR Code" style="max-width: 50%;height: auto;float: left;margin-right: 10px;margin-bottom: 2%;" readonly="true">
													<?php
													function get_web_page($url) {
														    $options = array(
														        CURLOPT_RETURNTRANSFER => true,   // return web page
														        CURLOPT_HEADER         => false,  // don't return headers
														        CURLOPT_FOLLOWLOCATION => true,   // follow redirects
														        CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
														        CURLOPT_ENCODING       => "",     // handle compressed
														        CURLOPT_USERAGENT      => "test", // name of client
														        CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
														        CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
														        CURLOPT_TIMEOUT        => 120,    // time-out on response
														    );

														    $ch = curl_init($url);
														    curl_setopt_array($ch, $options);

														    $content  = curl_exec($ch);

														    curl_close($ch);

														    return $content;
														}
														$response = get_web_page("http://103.163.139.199:8000/api-oauth-service/master/barcode/v1/status/".$product_en);
														$resArr = "Not Valid";
															if (isset(json_decode($response)->data)){
																$resArr = json_decode($response)->data;
															}
														$color = "red";
															if($resArr == "REGISTERED"){
																$color = "chocolate";
															}elseif ($resArr == "READY REGISTERED") {
																$color = "green";
															}

														$text = "Mohon Maaf Produk Anda Tidak Terdaftar.!!";
															if($resArr == "REGISTERED"){
																$text = "Selamat, Produk sudah terverivikasi.!!";
															}elseif ($resArr == "READY REGISTERED") {
																$text = "Product Anda Terdaftar, dan Terverifikasi oleh BPOM.";
															}
													?>
													<label>
														<?php echo $text; ?>
													</label>
											</div>
											<div class="field half" style="margin-top:10%;">
												<ul class="actions">
													<li><input type="button" value="<?php echo $resArr;?>" style="background-color:<?php echo $color;?>;font-weight: bold;font-size: large;" /></li>
												</ul>
											</div>
										</div>
									</div>
									<ul class="actions" style="margin-top:10%;">
										<?php if($resArr == "READY REGISTERED"){
												echo '<li><input type="button" id="regist" value="ACTIVATION" class="primary" /></li>';
										}else if($resArr == "REGISTERED"){
												echo '<li><input type="button" value="LOCK ACTIVATION" style="background-color:chocolate;" /></li>';
										}else{
												echo '<li><input type="button" value="NO ACCESS ACTIVATION" style="background-color:red;" /></li>';
										}?>
									</ul>
							</article>

							<script>
								$(document).ready(function() {
									var token = "";
							    $.ajax({
							        type: "POST",
							        url: 'http://103.163.139.199:8000/api-oauth-service/oauth/v1/signin',
							        contentType: 'application/json',
							        data: JSON.stringify({username: "admin", password: "admin"}) ,
							        success: function(data){
							          token = "Bearer "+data;
							        },
											error: function(){
												location.reload();
											}
							    });

									$("#regist").click(function(){
										$.ajax({
					              type: "GET",
					              url: 'http://103.163.139.199:8000/api-oauth-service/master/barcode/v1/registry/<?php echo $product_en;?>',
					              headers: {"Authorization": token},
					              success: function(resp){
													location.reload();
					              },
					              error: function(){
					                // location.reload();
					              }
					          });
									});
								});
							</script>

					</div>

				<!-- Footer -->
					<footer id="footer">
						<!-- <p class="copyright">&copy; Untitled. Design: <a href="https://html5up.net">HTML5 UP</a>.</p> -->
					</footer>

			</div>

		<!-- BG -->
			<div id="bg"></div>

		<!-- Scripts -->
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>
